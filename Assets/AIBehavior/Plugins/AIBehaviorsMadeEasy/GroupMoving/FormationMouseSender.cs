﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIBehavior;

public class FormationMouseSender : MonoBehaviour 
{
	public string unitMovingStateName;
	AIFormationMoving formationMovingScript;
	bool isDragging= false;
	Vector3 mouseDownPoint;
	Vector3 mouseUpPoint;
	Vector3 mouseDragStart;
	List<GameObject> selectableUnits = new List<GameObject>();

	void Start () 
	{
		formationMovingScript = GetComponent<AIFormationMoving> ();
		mouseDownPoint = Vector3.zero;
		mouseUpPoint = Vector3.zero;
		mouseDragStart = Vector2.zero;
		selectableUnits.AddRange(GameObject.FindGameObjectsWithTag ("Enemy"));
	}

	public void AddSelectableUnit(GameObject newUnit)
	{
		selectableUnits.Add (newUnit);
	}

	public void RemoveSelectableUnit(GameObject deadUnit)
	{
		selectableUnits.Remove (deadUnit);
	}

	void Update () 
	{
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

		if(Input.GetMouseButtonDown(0))
		{
			isDragging = true;
			mouseDragStart = Input.mousePosition;
			mouseDragStart.y = Screen.height - Input.mousePosition.y;

			// Raycast and save the worldPosition where the selection starts
			if(Physics.Raycast(ray, out hit, Mathf.Infinity))
			{
				mouseDownPoint = hit.point;
			}
		}
		if (Input.GetMouseButtonUp (0)) 
		{
			isDragging = false;
		
			// Raycast and save the worldPosition where the selection starts
			if(Physics.Raycast(ray, out hit, Mathf.Infinity))
			{
				mouseUpPoint = hit.point;
			}

			// Set selection bounds
			float minX = 0;
			float minZ = 0;
			float maxX = 0;
			float maxZ = 0;
			if (mouseDownPoint.x < mouseUpPoint.x)
			{
				minX = mouseDownPoint.x;
				maxX = mouseUpPoint.x;
			}
			else
			{
				minX = mouseUpPoint.x;
				maxX = mouseDownPoint.x;
			}

			if (mouseDownPoint.z < mouseUpPoint.z)
			{
				minZ = mouseDownPoint.z;
				maxZ = mouseUpPoint.z;
			}
			else
			{
				minZ = mouseUpPoint.z;
				maxZ = mouseDownPoint.z;
			}

			// Select units
			List<AIBehaviors> selectedAIs = new List<AIBehaviors>();

			for (int i = 0; i < selectableUnits.Count; i++) 
			{
				Vector3 unitPos = selectableUnits [i].transform.position;

				// Check each unit if it is within the selection bounds
				if (unitPos.x > minX && unitPos.x < maxX && unitPos.z > minZ && unitPos.z < maxZ) 
				{
					AIBehaviors newAI = selectableUnits [i].GetComponentInChildren<AIBehaviors> ();
					if (newAI != null)
						selectedAIs.Add (newAI);
				}
			}
			Debug.Log ("Total units: "+selectableUnits.Count+" - Selected: " + selectedAIs.Count);
			formationMovingScript.SetAIs (selectedAIs.ToArray());
		}
		if (Input.GetMouseButtonDown (1)) 
		{
			// Sending selected units
			if(Physics.Raycast(ray, out hit, Mathf.Infinity))
			{
				formationMovingScript.SendAIsTo (hit.point, unitMovingStateName);
			}
		}
	}

	void OnGUI()
	{
		if (isDragging) 
		{
			float xSize = Input.mousePosition.x -  mouseDragStart.x;
			float ySize = Screen.height - Input.mousePosition.y - mouseDragStart.y;
			GUI.Box (new Rect (mouseDragStart.x, mouseDragStart.y, xSize, ySize), "");
		}
	}
}
