﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Weapon : MonoBehaviour {

    private Animator anim;
    private AudioSource _AudioSource;
    public float range = 150f;
    public int bulletsPerMag = 30;
    public int magazinesLeft;
    public int currentBullets;
    public Transform shootPoint;
    public ParticleSystem muzzleFlash;
    public AudioClip shootSound;
    public Text bulletText;
    public float fireRate = 0.1f;
    public float damageAmount = 1;
    float fireTimer;
    int playerHealth = 200;
    public Text healthText;
    int killedEnemies = 0;
    public Text killedEnemiesText;
    public GameObject screenFlash;
    Scene currentScene;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        _AudioSource = GetComponent<AudioSource>();
        currentBullets = bulletsPerMag;
        UpdateBulletText();
        UpdateHealthText();      
    }

    // Update is called once per frame
    void Update () {
        currentScene = SceneManager.GetActiveScene();     
      
        if (Input.GetButton("Fire1"))
        {
            if(currentBullets > 0)
            {
                Fire(); 
            } else
            {
                DoReload();
            }
        }
		
        if (fireTimer < fireRate)
        {
            fireTimer += Time.deltaTime;
        }

        if(Input.GetKeyDown(KeyCode.R))
        {
            DoReload();
        }

        if (playerHealth <= 0)
        {
            UpdateKilledEnemies();
            GameOver();
        }       
    }

    void GameOver()
    {
        SceneManager.LoadScene(1);   
    }
 
     IEnumerator PlayerDamage(int damageAmount)
    {
        playerHealth -= damageAmount;
        screenFlash.SetActive(true);
        UpdateHealthText();

        yield return new WaitForSeconds(0.12f);
        screenFlash.SetActive(false);
    }

    void FixedUpdate()
    {
        AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(0);       
    }

    private void Fire()
    {
        if (fireTimer < fireRate || currentBullets <= 0)
        {
            return;
        }
       
        RaycastHit hit;

        if(Physics.Raycast(shootPoint.position, shootPoint.transform.forward, out hit, range))
        {
            hit.transform.SendMessage("DeductPoints", damageAmount, SendMessageOptions.DontRequireReceiver);
        }

        anim.CrossFadeInFixedTime("Fire", 0.01f);
        muzzleFlash.Play();
        PlayShootSound();

        anim.SetBool("Fire", true);
        currentBullets--;
        fireTimer = 0.0f; 
        UpdateBulletText();
    }

    private void DoReload()
    {
        AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(0);

        if (info.IsName("Reload")) return;

        anim.CrossFadeInFixedTime("Reload", 0.01f);

        if (magazinesLeft <= 0)
        {
            return;
        }

        magazinesLeft --;
        currentBullets = bulletsPerMag;
        UpdateBulletText();
    }

    private void PlayShootSound()
    {
        _AudioSource.PlayOneShot(shootSound);
   
    }

    private void UpdateBulletText()
    {
        bulletText.text = currentBullets + "/" + bulletsPerMag;
    }

    private void UpdateHealthText()
    {
        healthText.text = playerHealth + " hp";
    }

    private void UpdateKilledEnemies()
    {
        killedEnemiesText.text = "Killed: " + killedEnemies;
    }

    void SetKilledEnemies (int numberOfKilledEnemies)
    {
        killedEnemies += numberOfKilledEnemies;
        UpdateKilledEnemies();
    }   
}
